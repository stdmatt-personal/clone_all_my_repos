#!/usr/bin/env python3
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : main.py                                                       ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : April 04, 2021                                                ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2021, 2022                                          ##
##                                                                            ##
##  Description :                                                             ##
##    This project gets all the repositories that I work and clone them       ##
##    into my Projects folder. It tries to separate the things by folders     ##
##    so I have everything organized always ;D                                ##
##                                                                            ##
##    It accepts the project directory as the first parameter.                ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
import json;
import os;
import os.path;
import pdb;
import sys;
import urllib.request;


##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
## @TODO(stdmatt): Find a way to automatically get the groups that I'm into...
ARKADIA_GROUPS=[
    "arkadia_games",
];
STDMATT_GROUPS=[
    "stdmatt-demos",
    "stdmatt-libs",
    "stdmatt-personal",
    "stdmatt-games",
    "stdmatt-tools",
];

GITLAB_URL_FMT = "https://gitlab.com/api/v4/groups/{name}/projects?include_subgroups=true";
GITHUB_URL_FMT = "https://api.github.com/users/stdmatt/repos";

BASE_PATH = "~/Projects";


##----------------------------------------------------------------------------##
## Info                                                                       ##
##----------------------------------------------------------------------------##
PROGRAM_NAME            = "clone-all-my-repos";
PROGRAM_VERSION         = "1.1.0";
PROGRAM_AUTHOR          = "stdmatt - <stdmatt@pixelwizads.io>";
PROGRAM_COPYRIGHT_OWNER = "stdmatt";
PROGRAM_COPYRIGHT_YEARS = "2021, 2022";
PROGRAM_DATE            = "04 Apr, 2021";
PROGRAM_LICENSE         = "GPLv3";

##------------------------------------------------------------------------------
def show_version():
    msg = """{program_name} - {program_version} - {program_author}
Copyright (c) {program_copyright_years} - {program_copyright_owner}
This is a free software ({program_license}) - Share/Hack it
Check http://stdmatt.com for more :)""".format(
        program_name=PROGRAM_NAME,
        program_version=PROGRAM_VERSION,
        program_author=PROGRAM_AUTHOR,
        program_copyright_years=PROGRAM_COPYRIGHT_YEARS,
        program_copyright_owner=PROGRAM_COPYRIGHT_OWNER,
        program_license=PROGRAM_LICENSE,
    );
    print(msg);
    exit(0);


##----------------------------------------------------------------------------##
## Helper Functions                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def print_help():
    print("""Usage:
  {program_name} [-h | -v] [-f] [clone-dir]

Options:
  *-h --help     : Show this screen.
  *-v --version  : Show app version and copyright.

   -f --force    : Creates the clone directory if doesn't exists.

Notes:
  If [clone-dir] is blank the dir is assumed to be the default clone directory.
  ({default_dir})

  Options marked with * are exclusive, i.e. the {program_name} will run that
  and exit after the operation.
""".format(program_name=PROGRAM_NAME, default_dir=BASE_PATH));
    exit(0);

##------------------------------------------------------------------------------
def clone(url, path):
    if(os.path.isdir(path)):
        print("Project is already cloned:\n   URL:  {url}\n   Path: {path}\n".format(
            url=url,
            path=path
        ));
        return;

    print("Cloning Project:\n   URL:  {url}\n   Path: {path}\n".format(
        url=url,
        path=path
    ));

    os.makedirs(path, exist_ok=True);
    os.system("git clone --recurse-submodules {url} {path}".format(url=url, path=path));

##------------------------------------------------------------------------------
def clone_gitlab(clone_path, groups, folder_name):
    for group_name in groups:
        try:
            url      = GITLAB_URL_FMT.format(name=group_name);
            response = urllib.request.urlopen(url);
            data     = json.loads(response.read());
        except Exception as e:
            print("[FATAL] Failed to clone {}".format(group_name));
            continue;

        ## While the group name has the "stdmatt-" prefix we actually don't
        ## want this on the filesystem, but instead just the part that represents
        ## the type of the group.
        group_name_dir = os.path.join(
            clone_path,
            folder_name,
            group_name.replace("stdmatt-", ""),
        );

        for item in data:
            name      = item["name"];
            namespace = item["namespace"]["full_path"];
            repo_url  = item["http_url_to_repo"];

            ## If the namespace is not a subgroup of this group
            ## we don't want to have it's name because we're cloning
            ## and creating the directories based on the group's names already.
            namespace = os.path.basename(namespace);
            namespace = "" if namespace == group_name else namespace;

            clone_dir = os.path.join(group_name_dir, namespace, name);
            clone(repo_url, clone_dir);

##------------------------------------------------------------------------------
def clone_github(clone_path, folder_name):
    url      = GITHUB_URL_FMT;
    response = urllib.request.urlopen(url);
    data     = json.loads(response.read());

    clone_basepath = os.path.join(clone_path, folder_name);
    for item in data:
        name      = item["name"];
        clone_url = item["clone_url"];

        clone_fullpath = os.path.join(clone_basepath, name);
        clone(clone_url, clone_fullpath);


##----------------------------------------------------------------------------##
## Entry Point                                                                ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def main():
    args             = sys.argv[1:];
    clone_path       = BASE_PATH;
    force_create_dir = False;

    if(len(args) != 0):
        for arg in args:
            clean_arg = arg.lower();
            if  (clean_arg == "-h" or clean_arg == "--help"   ): print_help   ();
            elif(clean_arg == "-v" or clean_arg == "--version"): print_version();
            elif(clean_arg == "-f" or clean_arg == "--force"  ): force_create_dir = True;
            else:                                                clone_path       = arg;

    clone_path = os.path.abspath(os.path.expanduser(clone_path));
    if(force_create_dir and not os.path.isdir(clone_path)):
        os.makedirs(clone_path);
    else:
        if(not os.path.isdir(clone_path)):
            print("[FATAL] <clone-path> isn't a valid path ({})".format(clone_path));
            exit(1);

    ## @notice(stdmatt): 07 Dec, 2021 at 10:34:55
    ## Due the way that the repositories are strutured, arkadia stuff is actually
    ## a subproject of stdmatt, but we want the directory hierarchy to be as
    ## if them are in the same level, hence we are not passing a folder name
    ## making the clone function to clone their repos as if they were one level
    ## above.
    clone_gitlab(clone_path, ARKADIA_GROUPS, "");
    clone_gitlab(clone_path, STDMATT_GROUPS, "stdmatt");
    clone_github(clone_path,                 "third_party");


main();
