#!/usr/bin/env bash

##
## Constants
##
##------------------------------------------------------------------------------
## Script
SCRIPT_DIR=$(dirname `which $0`);
HOME_DIR="$HOME";
## Program
PROGRAM_NAME="clone-all-my-repos";
PROGRAM_SOURCE_PATH="$SCRIPT_DIR/clone_all_my_repos";
PROGRAM_INSTALL_PATH="$HOME_DIR/.stdmatt_bin/$PROGRAM_NAME";

##
## Script
##
##------------------------------------------------------------------------------
echo "Installing...";

## Create the install directory...
if [ ! -d "$PROGRAM_INSTALL_PATH" ]; then
    echo "Creating directory at: ";
    echo "    $PROGRAM_INSTALL_PATH";
    mkdir -p "$PROGRAM_INSTALL_PATH";
fi

## Copy the file to the install dir...
cp -f $PROGRAM_SOURCE_PATH/main.py $PROGRAM_INSTALL_PATH/$PROGRAM_NAME;
chmod 744 $PROGRAM_INSTALL_PATH/$PROGRAM_NAME;

echo "$PROGRAM_NAME was installed at:";
echo "    $PROGRAM_INSTALL_PATH";
echo "You might need add it to the PATH";
echo '    PATH=$PATH:'"$PROGRAM_INSTALL_PATH";

echo "Done... ;D";
echo "";
